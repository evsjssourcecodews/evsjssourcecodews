'use strict';
var selectedDigit;	//contains the current selected digit
var cells 				= {}; //defines an aa array holding content for each cell button (cell for short)
var valError;		//when true, the cell button didn't pass validation
var row;			//contains the cell button current row
var col;			//contains the cell button current column
var eraseFlag			= false; //when on: the erase button was clicked.
var initialValuesFlag	= false; //when on: the initial value button was clicked.
var myInputsFlag		= false; //when on: the my inputs button was clicked.
var triesFlag			= false; //when on: the try button was clicked.
var cellsInSubset		= []; /*creates a 2D array for cells in a subset.
 											 *numbers from 00 --> 26. This defines cells belonging
											 * to a specific subset.
											 */ 
var cellsInRow		= []; //2D array for cells in a row.
var cellsInColumn		= []; //2D array for cells in a column.
const digitFulFilled 	= []; //tracks how many times a digit is content into the cell buttons (exclude tries)
const rowFulfilled		= []; //tracks how many cell buttons in the row have been successfully filled
const colFulfilled		= []; //tracks how many cell buttons in the col have been successfully filled
const subsetFulfilled	= []; //tracks how many cell buttons in a subset contain info.
const specialBGColorClassSubsets = [1]; //tracks the subsets having cells with special BG color class.
var gameErrorFlag		= false; //turns on when errors come from the game (detected by js)
var specialButtonFlag	= false;
var wrongCellButton;
var cellContentSelector; 	//this is for the span tag
var cellClassSelector;		//this is for the button tag
const cellSelectorPre = '#form1 #cell';
const cellContentSelector2 = '#form1 :input';
const cellColorSubstring = 'BGColor';
const fontSizeAttribName = 'font-size';
const fontSizeAttribRegValue = '16px';
const initialValueButtonSelector = '#form2 #initialValueButton';
const myInputsButtonSelector = '#form2 #myInputsButton';
const triesButtonSelector = '#form2 #triesButton';
const eraseButtonSelector = '#form2 #eraseButton';
const digitButtonSelectorPre = '#form2 #button';
const yellowBG = 'setBGColorToYellow';
const greenBG = 'setBGColorToGreen';
const redBG = 'setBGColorToRed';
const specialBG = 'specialBGColor';
const regularBG = 'regularBGColor';
const smallFontSize = 'smallSize';
const regularFontSize = 'regularSize';
const notClassFound = 'false';
const blackText = 'blackCellText';
const blueText = 'blueCellText';
//var cellId  = 'cell' + 0; 

$(document).ready(() =>{
    //initialize the rowFulfilled, colFulfilled, subsetFulfilled, and digitFulFilled arrays.
	for (var i = 0; i <= 8; i++) {		
		rowFulfilled[i]			= 0;
		colFulfilled[i] 		= 0;
		subsetFulfilled[i] 		= 0;
		digitFulFilled[i + 1] 	= 0;
    }	
    
    //fill the cells, ***fulFilled arrays, but subsetFilled.
	updateArraysInfo();		 

	//build cells per row array
	fillSpecialBGColorCell();

	//check if a digit has 9 occurs.
	checkNineDigitOccurs();
	
	//click event handler for cell buttons.
	// for(let i = 0; i <= 17; i++){
 
		// cellId.addEventListener('click', () => {
		// 	updateCell(0);
		// })
	//}
    // boton.addEventListener('click', ()=> {
    //     cambiarColor();
	// })
	initialValueButton.addEventListener('click', ()=> {
        setInitialValuesBGColor();
	})
	myInputsButton.addEventListener('click', ()=> {
        setMyInputsBGColor();
	})
	triesButton.addEventListener('click', ()=> {
        setTriesBGColor();
	})
	eraseButton.addEventListener('click', ()=> {
        setEraseBGColor();
    })
}); // end ready

function create2DArray(rows) {
	const arr = [];
	for (let i = 0; i < rows; i++) {
	   arr[i] = [];
	}
	return arr;
}

function updateArraysInfo() {
	cellsInRow = create2DArray(3);
	cellsInColumn = create2DArray(6);
	cellsInSubset = create2DArray(2);	

    for (let i = 0; i <= 17; i++) {
		let tempArray = ['']; //these two statetments are no intended for jsf version.
		renderCellContent(cellSelectorPre + i, tempArray, true);
		
        /*builds an Array (cells with contents from each cell
        *and its font size built on objects
        */			
        var cellInfo 	  = new Object();
		cellInfo.content  = $(cellSelectorPre + i).val();
		if ($(cellSelectorPre + i).val().trim().length > 1) {
			$(cellSelectorPre + i).addClass(smallFontSize);
		} else {
			$(cellSelectorPre + i).addClass(regularFontSize);
		}
        cellInfo.fontsize = $(cellSelectorPre + i).css(fontSizeAttribName);
		let thereIsABGColorClass = findBGColorClasses(cellSelectorPre + i);
		if (thereIsABGColorClass == notClassFound) {
			$(cellSelectorPre + i).addClass(regularBG);
			cellInfo.BGColor = regularBG;
		} else {
			$(cellSelectorPre + i).addClass(thereIsABGColorClass);
			cellInfo.BGColor = thereIsABGColorClass;
		}	
		
		//set the text color for the cell.	
        cells[i] 		  = cellInfo;
        //update the rowFulfilled and colFulfilled arrays
        fillRowAndColumn(i, cells[i].content, cells[i].fontsize, false); 
        //update the digitFulFilled array
        if (cells[i].content.length != 0 &&
            cells[i].fontsize   	== fontSizeAttribRegValue) {
            digitFulFilled[cells[i].content]++;
		}
				
		locateCellInRowColumnSubset(i);
	}
	return true;
}

function fillRowAndColumn(cellId, cellInfoContent, cellInfoFontsize, highlightFlag) {
	findRowAndColumn(cellId);
	//update the rowFulfilled and colFulFilled arrays
	if (eraseFlag) {
		if (rowFulfilled[row] >= 1) {
			rowFulfilled[row]--;
		}
		if (colFulfilled[col] >= 1) {
			colFulfilled[col]--;			
		}
		return true;
	}
	if (cellInfoContent > 0 &&
		cellInfoFontsize  == fontSizeAttribRegValue) {
		rowFulfilled[row]++;
		colFulfilled[col]++;
		
		//highlighting rows
		if (highlightFlag &&
			rowFulfilled[row] == 6) {	
			highlightARow(row);
		}
		
		//highlighting columns
		if (highlightFlag &&
			colFulfilled[col] == 3) {	
			highlightACol(col);
		}	
	}
	return true;					
}

function findRowAndColumn(cellId) {	
	row = Math.floor(cellId/6);
	col	= cellId % 6;
	return true;
}

function analyzeCellContentForSubsets(subset, cellButton, highlightFlag ) {
	if (eraseFlag) {
		if (subsetFulfilled[subset] >= 1)
			subsetFulfilled[subset]--;
			return true;
	}
	if (cells[cellButton].content > 0 &&
		cells[cellButton].fontsize == fontSizeAttribRegValue) {
		subsetFulfilled[subset]++;
		
		//highlighting the subset
		if (highlightFlag &&
			subsetFulfilled[subset] == 9) {	
			highlightASubset(subset);
		}	
	}
	return true;
}

//if a digit has 9 occurs, set the digit button background to red
function checkNineDigitOccurs(selectedDigit) {
	if (selectedDigit != undefined) { //this happens when called from update updateCell()
		cellClassSelector = digitButtonSelectorPre + selectedDigit;
		if (digitFulFilled[selectedDigit] == 2) {
			if ($(cellClassSelector).hasClass(redBG)) {
				return true;
			}
			let thereIsABGColorClass = findBGColorClasses(cellClassSelector);			
			if (thereIsABGColorClass != notClassFound) {
				$(cellClassSelector).removeClass(thereIsABGColorClass);
			}			
			$(cellClassSelector).addClass(redBG);	
			return true;
		} else {			
			if ($(cellClassSelector).hasClass(redBG)) {
				$(cellClassSelector).removeClass(redBG)
				$(cellClassSelector).addClass(greenBG);	
				return true;
			}
			return true; 
		}
	}

	for (let i = 1; i <= 9; i++) { //this happens when called from $(document).ready()
		if (digitFulFilled[i] == 2 ) {
			cellClassSelector 	= digitButtonSelectorPre + i;
			if ($(cellClassSelector).hasClass(redBG)) {
				return true;
			}
			let thereIsABGColorClass = findBGColorClasses(cellClassSelector);			
			if (thereIsABGColorClass != notClassFound) {
				$(cellClassSelector).removeClass(thereIsABGColorClass);
			}
			$(cellClassSelector).addClass(redBG);
		}
	}
	return true;
}

/*to set the background color for the selected
 *digit button. It turns to green from yellow.
 */
function setDigitCellColor(digit) { //this is called by the webpage.
	selectedDigit = digit
	let buttonToColor = digitButtonSelectorPre + digit;
	if ($(buttonToColor).hasClass(redBG)) {
		return true;
	}
	//set bg color to the specified digit button.
	let thereIsABGColorClass = findBGColorClasses(buttonToColor);
			if (thereIsABGColorClass != notClassFound) {
				$(buttonToColor).removeClass(thereIsABGColorClass);
			}	
	$(buttonToColor).addClass(greenBG);	
	/*check the other digit buttons to remove the green bg color
	  if there is.
	  loop cells down the current one (digit)
	*/
	for ( let i = digit - 1; i > 0; i-- ) {
		buttonToColor = digitButtonSelectorPre + i;
		if ($(buttonToColor).hasClass(redBG)) {
			continue;
		}
		if ($(buttonToColor).hasClass(greenBG)) {
			$(buttonToColor).removeClass(greenBG);
			$(buttonToColor).addClass(yellowBG);
		}		
	}
	//loop cells up the current one (digit)	
	for ( let i = digit + 1; i < 10; i++ ) {
		buttonToColor = digitButtonSelectorPre + i;
		if ($(buttonToColor).hasClass(redBG)) {
			continue;
		}
		if ($(buttonToColor).hasClass(greenBG)) {
			$(buttonToColor).removeClass(greenBG);
			$(buttonToColor).addClass(yellowBG);
		}
	}
	
	/*check what cell buttons contain this digit and
	 *set the background color to green for these cells.
	 remove the green background color from cells that 
	 are have a different digit (not in red color, not my tries) and
	 have a green background color.
	 */
	checkCellsMatchingDigit(selectedDigit);
	return true;
}

/*check the bg color for each cell and take decisions.
 */
function checkCellsMatchingDigit(selectedDigit) {
	for (var i = 0; i <= 17; i++ ) {
		if ($(cellSelectorPre + i).hasClass(redBG) ||
			cells[i].fontsize 	!= fontSizeAttribRegValue ||
			($(cellSelectorPre + i).hasClass(greenBG) &&
			cells[i].content == selectedDigit) ||
			(!$(cellSelectorPre + i).hasClass(greenBG) &&
			cells[i].content != selectedDigit)) {
			continue;
		}
		if (cells[i].content != selectedDigit &&
			$(cellSelectorPre + i).hasClass(greenBG)) {
			$(cellSelectorPre + i).removeClass(greenBG);
			$(cellSelectorPre + i).addClass(cells[i].BGColor); //restores orig bg color	
		} else {
			let thereIsABGColorClass = findBGColorClasses(cellSelectorPre + i);
			if (thereIsABGColorClass != notClassFound) {
				$(cellSelectorPre + i).removeClass(thereIsABGColorClass);
			}			
			$(cellSelectorPre + i).addClass(greenBG);
		}
	}
	return true;	
}

/*to set the background color for initial value button 
 *it works as a toggle
 */
function setInitialValuesBGColor() {
	let thereIsABGColorClass = findBGColorClasses(initialValueButtonSelector);
			if (thereIsABGColorClass != notClassFound) {
				$(initialValueButtonSelector).removeClass(thereIsABGColorClass);
			}	
	$(initialValueButtonSelector).addClass(greenBG);
	if ($(myInputsButtonSelector).hasClass(greenBG)) {
		$(myInputsButtonSelector).removeClass(greenBG);
		$(myInputsButtonSelector).addClass(yellowBG);
	}
	if ($(triesButtonSelector).hasClass(greenBG)) {
		$(triesButtonSelector).removeClass(greenBG);
		$(triesButtonSelector).addClass(yellowBG);
	}
	if ($(eraseButtonSelector).hasClass(greenBG)) {
		$(eraseButtonSelector).removeClass(greenBG);
		$(eraseButtonSelector).addClass(yellowBG);
	}		
	initialValuesFlag   = true;
	myInputsFlag 		= false;
	triesFlag 			= false;
	eraseFlag 			= false;
	specialButtonFlag 	= true;
	return true;
}

/*to set the background color for my inputs button 
 *it works as a toggle
 */
function setMyInputsBGColor() {	
	let thereIsABGColorClass = findBGColorClasses(myInputsButtonSelector);
			if (thereIsABGColorClass != notClassFound) {
				$(myInputsButtonSelector).removeClass(thereIsABGColorClass);
			}	
	$(myInputsButtonSelector).addClass(greenBG);
	if ($(initialValueButtonSelector).hasClass(greenBG)) {
		$(initialValueButtonSelector).removeClass(greenBG);
		$(initialValueButtonSelector).addClass(yellowBG);
	}
	if ($(triesButtonSelector).hasClass(greenBG)) {
		$(triesButtonSelector).removeClass(greenBG);
		$(triesButtonSelector).addClass(yellowBG);
	}
	if ($(eraseButtonSelector).hasClass(greenBG)) {
		$(eraseButtonSelector).removeClass(greenBG);
		$(eraseButtonSelector).addClass(yellowBG);
	}
	initialValuesFlag   = false;
	myInputsFlag 		= true;
	triesFlag 			= false;
	eraseFlag 			= false;
	specialButtonFlag 	= true;
	return true;
}

/*to set the background color for tries button 
*it works as a toggle
*/
function setTriesBGColor() {	
	let thereIsABGColorClass = findBGColorClasses(triesButtonSelector);
			if (thereIsABGColorClass != notClassFound) {
				$(triesButtonSelector).removeClass(thereIsABGColorClass);
			}	
	$(triesButtonSelector).addClass(greenBG);
	if ($(myInputsButtonSelector).hasClass(greenBG)) {
		$(myInputsButtonSelector).removeClass(greenBG);
		$(myInputsButtonSelector).addClass(yellowBG);
	}
	if ($(initialValueButton).hasClass(greenBG)) {
		$(initialValueButton).removeClass(greenBG);
		$(initialValueButton).addClass(yellowBG);
	}
	if ($(eraseButtonSelector).hasClass(greenBG)) {
		$(eraseButtonSelector).removeClass(greenBG);
		$(eraseButtonSelector).addClass(yellowBG);
	}
	initialValuesFlag   = false;
	myInputsFlag 		= false;
	triesFlag 			= true;
	eraseFlag 			= false;
	specialButtonFlag 	= true;
	return true;
}

/*to set the background color for erase button 
*it works as a toggle
*/
function setEraseBGColor() {	
	let thereIsABGColorClass = findBGColorClasses(eraseButtonSelector);
			if (thereIsABGColorClass != notClassFound) {
				$(eraseButtonSelector).removeClass(thereIsABGColorClass);
			}	
	$(eraseButtonSelector).addClass(greenBG);
	if ($(myInputsButtonSelector).hasClass(greenBG)) {
		$(myInputsButtonSelector).removeClass(greenBG);
		$(myInputsButtonSelector).addClass(yellowBG);
	}
	if ($(triesButtonSelector).hasClass(greenBG)) {
		$(triesButtonSelector).removeClass(greenBG);
		$(triesButtonSelector).addClass(yellowBG);
	}
	if ($(initialValueButtonSelector).hasClass(greenBG)) {
		$(initialValueButtonSelector).removeClass(greenBG);
		$(initialValueButtonSelector).addClass(yellowBG);
	}
	initialValuesFlag   = false;
	myInputsFlag 		= false;
	triesFlag 			= false;
	eraseFlag 			= true;
	specialButtonFlag 	= true;
	return true;
}

//this function updates the cell object.
function updateCell(cellId) {
	/* if none of the special buttons has been clicked
	* leave the function.
	*/
	if (!specialButtonFlag) {
		var errSelector = $("#error-zone");
		errSelector.html("You need to click on a special button");
		errSelector.addClass('error-msg');
		removeMsg(errSelector);
		return true;
	}
	
	/* if a special buttons was clicked and no digit button
	has been clicked, leave the function.
	*/
	if (selectedDigit == undefined) {
		var errSelector = $("#error-zone");
		errSelector.html("You need to click on a digit button");
		errSelector.addClass('error-msg');
		removeMsg(errSelector);
		return true;
	}
	
	/*if a cell contains the same selectedDigit and 
	regularFontSize and the erase button hasn't been clicked:
	ignore and leave the function.
	*/
	if (cells[cellId].content == selectedDigit &&
		cells[cellId].fontsize == fontSizeAttribRegValue &&
		!eraseFlag) {
		return true;
	}
	
	/*if there is an error, and 
	 * the erase button hasn't been clicked and
	 * the cellId is different to the cell button in error then
	 * skip the update. Doesn't allow inputs.
	 */	
	if (gameErrorFlag &&
	   (!eraseFlag ||
		cellId != wrongCellButton)) {	
		return true;
	}
	
	/*if erase button has been clicked and
	* the cell button is empty then leave the function
	*/ 
	if (cells[cellId].content.length == 0 &&
		eraseFlag) {
		return true;
	}

	cellContentSelector = cellSelectorPre + cellId;
	cellClassSelector = cellSelectorPre + cellId;

	//validate the cell button
	valError = false;
	if (!eraseFlag &&
		!triesFlag) {
		//validateDigit(cellId, cellClassSelector);
	}

	if (valError) {
		gameErrorFlag = true;
		//passes the errorFlag to the bean
		wrongDecision( [ { name: 'errorFlag', value: 'true' } ] );
		//activates a message in the bean
		errorMessage();
		//update the cell button content 
		$(cellContentSelector).text(selectedDigit);
		cells[cellId].content = selectedDigit;
		return true;
	}
	
	//update the cell button content when no errors
	setSpecificCellProperties(cellContentSelector, cellClassSelector, cellId)
	//update the cells[ ] because it passed validation
	cells[cellId].fontsize	= $(cellClassSelector).css(fontSizeAttribName);
	var tempCellId  		= cellId.toString();
	//pass cell id and cell button content to the bean
	//no server access at this time.
	// cellButtonContent([{ name: 'cellId', 	  value: tempCellId},
	// 				   { name: 'cellContent', value: cells[cellId].content}]);
	
	if (cells[cellId].fontsize == fontSizeAttribRegValue) {
		/*check if it needs to change the background color of this cell
		 *based in that there is at least one cell containing the same digit
		 */
		if (digitFulFilled[selectedDigit] >= 1 &&
			!eraseFlag) {
			checkCellsMatchingDigit(selectedDigit)
		}

		//update the digitFulFilled array
		if (eraseFlag) {
			if (gameErrorFlag) {
				//try to turn off the error flag
				gameErrorFlag = false;
				wrongDecision( [ { name: 'errorFlag', value: 'false' } ] );
				return true;
			} else {
				if (digitFulFilled[selectedDigit] >= 1) {
					digitFulFilled[selectedDigit]--;
					console.log('current count for: ' + selectedDigit + ': ' + digitFulFilled[selectedDigit]);
					checkCellsMatchingDigit(selectedDigit);
				} 	
			}							
		} else {
			digitFulFilled[selectedDigit] = checkHowManyCellsContainThisDigit(selectedDigit);
			console.log('current count for: ' + selectedDigit + ': ' + digitFulFilled[selectedDigit]);
		}		
		//check if a digit has 9 occurs.
		checkNineDigitOccurs(selectedDigit);
		
		//update the rowFulfilled and colFulfilled arrays
		fillRowAndColumn(cellId, cells[cellId].content, cells[cellId].fontsize, true);
		//this loop updates fulfilled subsets
		for (var i = 0; i <= 2; i++) {
			if(cellsInSubset[i].includes(cellId)) {				
				analyzeCellContentForSubsets(i, cellId, true);
				break;
			}
		}
		
		//turn off the error flag
		gameErrorFlag = false;
		//no server access at this time.
		//wrongDecision( [ { name: 'errorFlag', value: 'false' } ] );
	}
	return true;
}

function validateDigit(cellId, cellClassSelector) {
	wrongCellButton = null;
	findRowAndColumn(cellId);
	var start	= row * 3;
	var end		= start + 2;
	$(cellClassSelector).removeClass(redBG);
	//check if digit is duplicated in its row
	for ( var i = start; i <= end; i++ ) {
		if (i == cellId) {
			continue;
		}
		if (cells[i].content == selectedDigit) {
			$(cellClassSelector).removeClass(cells[i].BGColor);						
			$(cellClassSelector).addClass(redBG);
			wrongCellButton = cellId;
			valError = true;
			return true;
		}			
	}
	//check if digit is duplicated in its column
	var start	= col;
	var end		= 80 - (8 - col);
	for ( var i = start; i <= end; i = i + 9) {
		if (i == cellId) {
				continue;
		}
		if (cells[i].content == selectedDigit) {			
			$(cellClassSelector).removeClass(cell[i].BGColor);
			$(cellClassSelector).addClass(redBG);
			wrongCellButton = cellId;
			valError = true;
			return true;
		}			
	}	
	/*check if digit is duplicated in its subset of cells
	 *find the block
	 */
	for (var i = 0; i <= 8; i++) {
		if(cellsInSubset[i].includes(cellId)) {
			var blockNbr = i;
			break;
		}
	}
		
	//go inside of the subset and look for duplicates
	for (var i = 0; i <= 8; i++) {
		if (cellsInSubset[blockNbr][i] == cellId ) {
			continue;
		}
		if (cells[cellsInSubset[blockNbr][i]].content  == selectedDigit) {			
			$(cellClassSelector).removeClass(cells[cellId].BGColor);
			$(cellClassSelector).addClass(redBG);
			wrongCellButton = cellId;
			valError = true;
			return true;
		}			
	}
	return true;	
}

function highlightARow(row) {
	//add the highlighting class for the whole row after 200 ms.				
	for (let i = 0; i <= 5; i++) {
		let cellClassSelector = cellSelectorPre + ((row * 6) + i);
		let thereIsABGColorClass = findBGColorClasses(cellClassSelector);
			if (thereIsABGColorClass != notClassFound) {
				$(cellClassSelector).removeClass(thereIsABGColorClass);
			}	
		$(cellClassSelector).addClass('highlightElements');				
	}
	$('.highlightElements').animate({backgroundColor: '#00ffff'}, 200);
	//remove the highlighting class after 1 second delay
	setTimeout (() => {
		for (let i = 0; i <= 5; i++) {					
			let cellClassSelector = cellSelectorPre + ((row * 6) + i);			
			$(cellClassSelector).removeClass('highlightElements');			
			$(cellClassSelector).css("background-color", "");
			if (cells[(row * 6) + i].content == selectedDigit) {
				$(cellClassSelector).addClass(greenBG)
			} else {
				$(cellClassSelector).addClass(cells[(row * 6) + i].BGColor);
			}																		
		}
	}, 1000);	
	checkCellsMatchingDigit(selectedDigit);
	return true;			
}

function highlightACol(col) {
	//add the highlighting class for the whole column after 200 ms.			
	for (let i = 0; i <= 2; i++) {
		let cellClassSelector = cellSelectorPre + ((i * 6) + col);
		let thereIsABGColorClass = findBGColorClasses(cellClassSelector);
			if (thereIsABGColorClass != notClassFound) {
				$(cellClassSelector).removeClass(thereIsABGColorClass);
			}	
		$(cellClassSelector).addClass('highlightElements');		
	}
	$('.highlightElements').animate({backgroundColor: '#00ffff'}, 200);
	//remove the highlighting class after 1 second delay
	setTimeout (() => {
		for (let i = 0; i <= 2; i++) {					
			let cellClassSelector = cellSelectorPre + ((i * 6) + col);
			$(cellClassSelector).removeClass('highlightElements');					
			$(cellClassSelector).css("background-color", "");
			if (cells[(i * 6) + col].content == selectedDigit) {
				$(cellClassSelector).addClass(greenBG)
			} else {
				$(cellClassSelector).addClass(cells[(i * 6) + col].BGColor);
			}
		}	
	}, 1000);	
	checkCellsMatchingDigit(selectedDigit);
	return true;
}

function highlightASubset(subset) {
	//add the highlighting class for the whole subset after 200 ms.	
	for (let i = 0; i <= 8; i++) {
		let cellClassSelector = cellSelectorPre + cellsInSubset[subset][i];
		let thereIsABGColorClass = findBGColorClasses(cellClassSelector);
		if (thereIsABGColorClass != notClassFound) {
			$(cellClassSelector).removeClass(thereIsABGColorClass);
		}
		$(cellClassSelector).addClass('highlightElements');
		
	}
	$('.highlightElements').animate({backgroundColor: '#00ffff'}, 200);
	//remove the highlighting class after 1 second delay
	setTimeout (() => {
		for (let i = 0; i <= 8; i++) {					
			let cellClassSelector = cellSelectorPre + cellsInSubset[subset][i];
			$(cellClassSelector).removeClass('highlightElements');					
			$(cellClassSelector).css("background-color", "");
			if (cells[cellsInSubset[subset][i]].content == selectedDigit) {
				$(cellClassSelector).addClass(greenBG)
			} else {
				$(cellClassSelector).addClass(cells[cellsInSubset[subset][i]].BGColor);
			}
		}
	}, 1000);
	checkCellsMatchingDigit(selectedDigit);
	return true;		
}

function setSpecificCellProperties(cellContentSelector, cellClassSelector, cellId) {
	//initial values and my inputs
	if (initialValuesFlag || myInputsFlag) {
		//set the regular font size.
		if ($(cellClassSelector).hasClass(smallFontSize)) {
			$(cellClassSelector).removeClass(smallFontSize);
		}				
		$(cellClassSelector).addClass(regularFontSize)
		let thereIsABGColorClass = findBGColorClasses(cellClassSelector);
		if (thereIsABGColorClass != notClassFound) {
			$(cellClassSelector).removeClass(thereIsABGColorClass);
		}	
		$(cellClassSelector).addClass(greenBG);
		$(cellContentSelector + ' div').remove();
		
		let tempArray = [selectedDigit];
		renderCellContent(cellContentSelector, tempArray, true);
		cells[cellId].content = selectedDigit;
		if (myInputsFlag) {
			$(cellContentSelector).css('color', 'blue');
		} else {
			$(cellContentSelector).css('color', 'black');
		}
		return true;
	}
	
	//tries
	if (triesFlag) {
		if ($(cellClassSelector).hasClass(regularFontSize)) {
			$(cellClassSelector).removeClass(regularFontSize);
		}
		$(cellClassSelector).addClass(smallFontSize);
		if (cells[cellId].content.length == 0) {
			$(cellContentSelector + ' div').remove();
			let tempArray = [selectedDigit];
			renderCellContent(cellContentSelector, fillMultiCellContent(tempArray), false);
			cells[cellId].content = selectedDigit;
			return true;
		}
		
		buildContent(cellContentSelector, cellId);
		return true;
	}		
	
	/*when there is only one digit, just remove it and return
	 * if it matches against the selected digit
	 */
	if (cells[cellId].content.length == 1 &&
		cells[cellId].content == selectedDigit) {
		cells[cellId].content = '';		
		$(cellContentSelector + ' div').remove();		
		
		//check the gameErrorFlag and set the original cell button bg
		if (gameErrorFlag) {
			$(cellClassSelector).removeClass(redBG);					
		}
		
		//if the cell button has green bg color, remove it
		if ($(cellClassSelector).hasClass(greenBG)) {
			$(cellClassSelector).removeClass(greenBG);
		}
		//restore the original bg color
		$(cellClassSelector).addClass(cells[cellId].BGColor);
		return true;
	}	
	
	//parse the current content.
	let tempContent = cells[cellId].content.toString().split("");
	
	//remove the element if it matches
	for( let i = 0; i < tempContent.length; i++){ 
		   if ( tempContent[i] == selectedDigit) {
			tempContent.splice(i, 1); 
			break;
		   }
	}	
	$(cellContentSelector + ' div').remove();
	renderCellContent(cellContentSelector, fillMultiCellContent(tempContent), false);
	cells[cellId].content = tempContent.join("");
	return true;
}

function buildContent(cellContentSelector, cellId) {	
	//parse the actual cell button content
	let tempArray = cells[cellId].content.toString().split("");
	//add the new element
	tempArray.push(selectedDigit.toString());
	//sort it
	tempArray.sort();
	//remove duplicates
	var arrayWithoutDuplicates = tempArray.filter((v, i, a) => a.indexOf(v) === i); 
	//update the cell button content
	$(cellContentSelector + ' div').remove();
	renderCellContent(cellContentSelector, fillMultiCellContent(arrayWithoutDuplicates), false);
	cells[cellId].content = arrayWithoutDuplicates.join("");
	return true;
}

function findBGColorClasses(cellClasses) {
    let classList = $(cellClasses)[0].className.split(' ');
	let BGColorClass = "";
	for (let el of classList) {
		if (el.search(cellColorSubstring) != -1) {
			BGColorClass = el;
			break;
		}
	}
	if (BGColorClass != "") {
		return BGColorClass;
	} else {
		return notClassFound;
	}
}

function removeMsg(errSelector) {
	setTimeout (() => {					
			$(errSelector).removeClass('error-msg');			
			errSelector.html("");
	}, 2000);
	return true;
}

function checkHowManyCellsContainThisDigit(selectedDigit ) {
    var acum = 0;    
	$.each(cells, (key, object) => {
		if(object.content == selectedDigit) {
			acum++;
		}
	})
    return acum;
}

function fillSpecialBGColorCell() {
	$.each(specialBGColorClassSubsets, (k, element) => {
		$.each(cellsInSubset[element], (k2, element2) => {			
			let cellClassSelector = cellSelectorPre + element2;
			let thereIsABGColorClass = findBGColorClasses(cellClassSelector);
			if (thereIsABGColorClass == regularBG) {
				$(cellClassSelector).removeClass(regularBG);
				$(cellClassSelector).addClass(specialBG);
				cells[element2].BGColor = specialBG;
			}		
		});
	});
	return true;
}

function locateCellInRowColumnSubset(cellId) {
	findRowAndColumn(cellId);	
	cellsInRow[row].push(cellId);
	cellsInColumn[col].push(cellId);
	if (row <= 2) {
		if (col <= 2) {
			cellsInSubset[0].push(cellId)
		} else {
			cellsInSubset[1].push(cellId)
		}
	} else {
		if (col <= 2) {
			cellsInSubset[0].push(cellId)
		} else {
			cellsInSubset[1].push(cellId)
		}
	}
	return true;
}

function renderCellContent(cellId, cellContent, isSingle) {
	let mainDiv = document.createElement('div');
	switch (isSingle) {
		case true:
			mainDiv.setAttribute('class', 'singleDigit');
			let paragraph = document.createElement("p");
			$(cellId).append(mainDiv);
    		paragraph.append(cellContent);
			$(mainDiv).append(paragraph);
			break;
		case false:
			let cellClass = 'element';
    		mainDiv.setAttribute('class', 'multiDigit');
			$(cellId).append(mainDiv);			
			$.each(cellContent, (index, element) => {
				let paragraph = document.createElement("p");
				paragraph.setAttribute('class', cellClass);
				if(element == null){
					paragraph.append(' ');
				} else {
					paragraph.append(element);
				}				
				$(mainDiv).append(paragraph);
			});
	}		
	return true;
}

function fillMultiCellContent(littleArray) {
	let multipleCellContent = new Array(9).fill(null);

	$.each(littleArray, (i, element1) => {
		$.each(multipleCellContent, (k, element2) => {
			if(k == element1 - 1) {
				multipleCellContent[k] = element1;
				return false;						
			}
		});
	});
	return multipleCellContent;
}