'use strict'
$("#gamesTable").dataTable({
    "aaData":[
      ["1953", "In progress", "2021-05-05 00:00:00", null, "A", "Play"]      
    ],
    "aoColumnDefs":[{
          "sTitle":"Game #"
        , "aTargets": [ "site_name" ]
    },{
          "aTargets": [ 1 ]        
    },{
          "aTargets":[ 2 ]
        , "sType": "date"
        , "mRender": function(date, type, full) {
            return new Date(date).toDateString();
        }  
    },{
        "aTargets":[ 3 ]
        , "sType": "date"
        , "mRender": function(date, type, full) {
            return (full[3] != null) 
            ? new Date(date).toDateString()
            : "---" ;
      }  
    },{
        "aTargets": [ 5]
      , "bSortable": false
      , "mRender": function ( url, type, full )  {
          return  '<a href="#">' + url + '</a>';
      }
  }]
  });