var selectedDigit;	//contains the current selected digit
var cells 				= {}; //defines an aa array holding content for each cell button (cell for short)
var valError;		//when true, the cell button didn't pass validation
var row;			//contains the cell button current row
var col;			//contains the cell button current column
var eraseFlag			= false; //when on: the erase button was clicked.
var initialValuesFlag	= false; //when on: the initial value button was clicked.
var myInputsFlag		= false; //when on: the my inputs button was clicked.
var triesFlag			= false; //when on: the try button was clicked.
const blockArray		= create2DArray(3); /*creates a 2D array (blockArray[][])
 											 *numbers from 00 --> 26. This defines cell buttons belonging
											 * to a specific subset. In other words.
											 */ 

const digitFulFilled 	= []; //tracks how many times a digit is content into the cell buttons (exclude tries)
const rowFulfilled		= []; //tracks how many cell buttons in the row have been successfully filled
const colFulfilled		= []; //tracks how many cell buttons in the col have been successfully filled
const subsetFulfilled	= []; //tracks how many cell buttons in a subset contain info.
var gameErrorFlag		= false; //turns on when errors come from the game (detected by js)
var specialButtonFlag	= false;
var wrongCellButton;
var cellSelector;			//this is for the button tag
var cellContentSelector; 	//this is for the span tag
const cellSelectorPre = '#form1 #cell';
const cellColorSubstring = 'BGColor';
const fontSizeAttribName = 'font-size';
const fontSizeAttribRegValue = '24px';

function create2DArray(rows) {
	  const arr = [];
	  for (var i = 0; i< rows; i++) {
	     arr[i] = [];
	  }
	  return arr;
}

$(document).ready(function(){
    //initialize the rowFulfilled, colFulfilled, subsetFulfilled, and digitFulFilled arrays.
	for (var i = 0; i <= 8; i++) {		
		rowFulfilled[i]			= 0;
		colFulfilled[i] 		= 0;
		subsetFulfilled[i] 		= 0;
		digitFulFilled[i + 1] 	= 0;
    }	
    
    //fill the cells, ***fulFilled arrays, but subsetFilled.
    updateArraysInfo();        
});

function updateArraysInfo() {		 
    for (var i = 0; i <= 8; i++) {
        cellContent        = $(cellSelectorPre + i);
        
        if ($(cellContent).val() == '') {
            $(cellContent).val(' ');
        }
        
        /*builds an Array (cells with contents from each cell
        *and its font size built on objects
        */
        var cellInfo 	  = new Object();
        cellInfo.content  = $(cellContent).val();
        cellInfo.fontsize = $(cellContent).css(fontSizeAttribName);
        cellInfo.BGColor = findBGColorClasses(cellSelectorPre + i);
        			
        cells[i] 		  = cellInfo;
        //update the rowFulfilled and colFulfilled arrays
        fillRowAndColumn(i, cells[i].content, cells[i].fontsize, false); 
        //update the digitFulFilled array
        if (cells[i].content.trim() != '' &&
            cells[i].fontsize   	== fontSizeAttribRegValue) {
            digitFulFilled[cells[i].content]++;
        }		
    };
}

function fillRowAndColumn(cellId, cellInfoContent, cellInfoFontsize, highlightFlag) {
	findRowAndColumn(cellId);
	//update the rowFulfilled and colFulFilled arrays
	if (eraseFlag) {
		if (rowFulfilled[row] >= 1) {
			rowFulfilled[row]--;
		}
		if (colFulfilled[col] >= 1) {
			colFulfilled[col]--;			
		}
		return;
	}
	if (cellInfoContent.trim().length > 0 &&
		cellInfoFontsize   	          == fontSizeAttribRegValue) {
		rowFulfilled[row]++;
		colFulfilled[col]++;
		
		//highlighting rows
		if (highlightFlag &&
			rowFulfilled[row] == 9) {	
			highlightARow(row);
		}
		
		//highlighting columns
		if (highlightFlag &&
			colFulfilled[col] == 9) {	
			highlightACol(col);
		}	
	}					
}

function findRowAndColumn(cellId) {	
	row = Math.floor(cellId/3);
	col	= cellId % 3;
}

function findBGColorClasses(cellClasses) {
    var classList = $(cellClasses)[0].className.split(' ');
    for (var i = 0; i <= classList.length; i++) {       
        if (classList[i].search(cellColorSubstring) > 0) {
            return classList[i];
        }
    }
}