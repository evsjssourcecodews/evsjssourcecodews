'use strict';
// // this is an example from JSONPlaceholder.
// //syntax 1 (the best):
// fetch('https://jsonplaceholder.typicode.com/todos/1')
//   .then(response => response.json())
//   .then(json => console.log(json));

// //syntax 2:
// fetch('https://jsonplaceholder.typicode.com/todos/1')
//   .then(function(response) {
//         return response.json()
//   })
//   .then(function(json){
//         return console.log(json);
//   });

// //this fails because: Reason: CORS header ‘Access-Control-Allow-Origin’ missing.
// //this domain has been changed.
// fetch('http://example.com/movies.json')
//     .then(response => response.json())
//     .then(data => console.log(data));  
// //////////////////////////////////////////////////////////////////////////////////////////

// // Example of POST method implementation:
// async function postData(url = '', data = {}) {
//     // Default options are marked with *
//     const response = await fetch(url, {
//       method: 'POST', // *GET, POST, PUT, DELETE, etc.
//       mode: 'cors', // no-cors, *cors, same-origin
//       cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
//       credentials: 'same-origin', // include, *same-origin, omit
//       headers: {
//         'Content-Type': 'application/json'
//         // 'Content-Type': 'application/x-www-form-urlencoded',
//       },
//       redirect: 'follow', // manual, *follow, error
//       referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
//       body: JSON.stringify(data) // body data type must match "Content-Type" header
//     });
//     return response.json(); // parses JSON response into native JavaScript objects
//   }
  
// //postData('https://example.com/answer', { answer: 42 }) //fails because the site is private
// //this creates a record.
// postData('https://jsonplaceholder.typicode.com/todos', { userId: 1953 })
// .then(data => {
//     console.log(data); // JSON data parsed by `data.json()` call
// });
// //////////////////////////////////////////////////////////////////////////////////////////

// //from common errors:
// //basic function. it doesn't catch errors.
// function showStuff() {
//     fetch("https://jsonplaceholder.typicode.com/todos/1953") //wrong id on purpose (404: not found).
//         .then(response => response.json())
//         .then(data => {
//             // ...show the stuff from `data`
//             console.log(data);
//         });
// }
// showStuff();

// //it catches http level errors.
// //option 1:
// function showStuff() {
//     fetch("https://jsonplaceholder.typicode.com/todos/1953") //wrong id on purpose (404: not found).
//         .then(response => {
//             if (!response.ok) {
//                 throw new Error("2nd. fail with HTTP code " + response.status);
//             }
//             return response;
//         })
//         .then(result => result.json())
//         .then(data => {
//             // ...show the stuff from `data`
//             console.log(data);
//         });
// }
// showStuff();

// //it catches http level errors.
// //option 2:
// function myFetch(...args) {
//     return fetch(...args).then(response => {
//         if (!response.ok) {
//             throw new Error("3rd. fail with HTTP code " + response.status);
//         }
//         return response;
//     });
// }
// function showStuff() {
//     myFetch("https://jsonplaceholder.typicode.com/todos/1953") //wrong id on purpose (404: not found).
//         .then(result => result.json())
//         .then(data => {
//             // ...show the stuff from `data`
//             console.log(data);
//         });
// }
// showStuff();

//catching all errors:
// //option 3: the best way.
// function myFetch(...args) {
//     return fetch(...args).then(response => {
//         if (!response.ok) {
//             throw new Error("4th. fail with HTTP code " + response.status);
//         }
//         return response;
//     });
// }
// function showStuff() {
//     myFetch("https://jsonplaceholder.typicode.com/todos/") //wrong id on purpose (404: not found), also 
//                                                         //it catches url mispellings.
//         .then(result => result.json())
//         .then(data => {
//             // ...show the stuff from `data`
//             console.log(data);
//         })
//         .catch(err => {
//             // ...do something with the error that occurred
//             console.log('something was wrong: ' + err)
//         });
// }
// showStuff();

//this builds my request object outside of the fetch(). It works.
// const myHeaders = new Headers();
// const myRequest = new Request('https://jsonplaceholder.typicode.com/todos', {
//   headers: myHeaders,
//   mode: 'cors',
//   cache: 'default',
// });

// fetch(myRequest)
//     .then(response => response.json())
//     .then(json => console.log(json));
////////////////////////////////////////////////////////////////////////////////////////////

// //works with the spring rest service
// function showStuff() {
//     fetch("http://localhost:8080/users")
//         .then(response => {
//             if (!response.ok) {
//                 throw new Error("Failed with HTTP code " + response.status);
//             }
//             return response;
//         })
//         .then(result => result.json())
//         .then(data => {
//             // ...show the stuff from `data`
//         });
// }
// showStuff();
//trying my java services
//alert('i am here');
//const myHeaders = new Headers();
//myHeaders.append('Content-Type', 'application/json');
// const myRequest = new Request('http://localhost:8080/users', {
//   method: 'GET',
//   headers: {'Content-Type' : 'application/json',},
//   mode: 'no-cors',
//   cache: 'default',
// });

// fetch(myRequest)
//     .then(response => response.json())
//     .then(json => console.log(json));

// fetch('/users', {
//   method: 'GET', 
//   headers: {
//     'Content-Type': 'application/json',
//   },
//   mode: 'no-cors',
// })
// fetch('/users', {
//     mode: 'no-cors',
// })
//     .then(response => {
//         if (!response.ok) {
//             throw new Error("Failed with HTTP code " + response.status);
//         }
//         return response;
//     })
//     .then(json => console.log(json))
//     .catch((error) => {
//         console.error('Error:', error);
//     });

// async function getData(url) {
// // Default options are marked with *
// const response = await fetch(url, {
//                                     mode: 'no-cors'
//                             });
// // if (!response.ok) {
// //     throw new Error("Failed with HTTP code " + response.status);
// // }                            
// return response.json(); // parses JSON response into native JavaScript objects
// }

// getData('/users')
// .then(data => {
//     console.log(data); // JSON data parsed by `data.json()` call
// // .catch((error) => {
// //     console.error('Error:', error);
// });



// const myHeaders = new Headers();
// const myRequest = new Request('http://localhost:8080/users', {
//   headers: myHeaders,
//   mode: 'no-cors'
// });

// fetch(myRequest)
//     .then(response => {
//         if (!response.ok) {
//             throw new Error("Failed with HTTP code " + response.status);
//         }
//         return response;
//     })
//     //.then(json => console.log(json));
//     .then(json => console.log(json))
//     .catch((error) => {
//         console.error('Error:', error);
//     });