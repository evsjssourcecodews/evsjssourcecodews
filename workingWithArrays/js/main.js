'use strict'
// const fruits = [] 
// //fruits = 'citrus' // throws error — cannot mutate (array to string) 
// fruits[0] = 'Apricot' // add item to index 0 (first position) 
// //fruits = 12 // throws error — cannot mutate (array to number) 
// fruits.push('Banana') // add item to end of array 
// //const fruits = [] // throws error — cannot re-declare a const 
//                   //(even it doesn't recognize the script)  
// fruits.unshift('Apple') // add item to beginning of array
// console.log(fruits);

const colors = ['white', 'black', 'custom'];
console.log(colors);

$(() => { //this ensures that the dom has been loaded.
    $('#colors').text(colors[0]);
});

const colors2 = new  Array ('white', 'black', 'custom');
console.log(colors2);