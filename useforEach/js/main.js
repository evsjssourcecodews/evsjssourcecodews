// [1,2,3].forEach((el) => {     
//     if(el === 2) {       
//         console.log(`Match on 2!`);
//         return;     
//     }     
//     console.log(el); //not executed when el = 2
// }); 

// //this works
// let BreakException = {};
// try {
//   [1, 2, 3].forEach((el) => {
//     console.log(el);
//     if (el === 2) throw BreakException;
//   });
// } catch (e) {
//   if (e !== BreakException) throw e;
// }

// //this works
// [1, 2, 3].some((el) => {
//     console.log(el);
//     return el === 2;
//   });

// //this works
// [2, 1, 3].every((el) => {
//     console.log(el);
//     return el === 2;
// });

// //this works
let arr = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// for (let el of arr) {
//   console.log(el);
//   if (el === 5) {
//     break;
//   }
// }

// //this works
// //if you need to work with the index:
// for (const [index, el] of arr.entries()) {
//     console.log(el);
//     if ( index === 5 ) {
//         break;
//     } 
// }

// //this works with jquery
// $.each([1, 2, 3], (index, el) => { 
//     if (el === 2) {
//         return false;
//     }    
//     console.log(el)
// })