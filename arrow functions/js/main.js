'use strict'

//with multiple parameters.
//traditional way.
function sum(a, b){
    return a + b
}
//console.log(sum(5, 4))

//arrow way.
let sum2 = (a, b) => a + b
//console.log(sum2(7, 3))

//with 1 parameter.
//traditional way.
function isPositive(number){
    return number >= 0
}
//console.log(isPositive(-7))

//arrow way.
let isPositive2 = number => number >= 0
//console.log(isPositive2(-7))

//with no parameters.
//traditional way.
function randomNumber(){
    return Math.random()
}
//console.log(randomNumber())

//arrow way.
let randomNumber2 = () => Math.random()
//console.log(randomNumber2())


//anonymous function.
//traditional way.
document.addEventListener('click', function(){
    //console.log('click')
})

//arrow way.
//document.addEventListener('click', () => console.log('click'))

//the 'this' keyword issue.
class Person {
    constructor(name) {
        this.name = name
    }

    printNameArrow() {
        setTimeout(() => {
            console.log('arrow: ' + this.name)
        }, 100)
    }

    printNameFunction() {
        setTimeout(function() {
            console.log('function: ' + this.name) //this.name is not recognized
                                                  //because person was defined @ global scope
        }, 100)
    }
}

let person = new Person('nicholas')
//person.printNameArrow()
//person.printNameFunction() //defined @ the global scope.
//console.log('content of this: ' + this.name) //this.name is not @ global scope