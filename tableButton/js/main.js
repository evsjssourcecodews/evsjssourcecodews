'use strict';
var cellContentSelector;
let whiSpace = '&nbsp';
const cellSelectorPre = '#form1 #cell';
let multipleCellContent = [1,2,3,4,5,6,7,8,9];
let singleCellContent = 7;


$(document).ready(() => {
// cellContentSelector = cellSelectorPre + 0;
// buildTable('cell0', cellContent);
// cellContentSelector = cellSelectorPre + 1;
// buildTable('cell1', ['','',3,'','','',7,'','']);
cellContentSelector = cellSelectorPre + 2;
renderTries('#cell2', multipleCellContent);
cellContentSelector = cellSelectorPre + 3;
renderTries('#cell3', [whiSpace,whiSpace,3,whiSpace,whiSpace,whiSpace,7,whiSpace,whiSpace]);
cellContentSelector = cellSelectorPre + 4;
renderRegularCell('#cell4', singleCellContent);
// cellContentSelector = cellSelectorPre + 5;
// buildTable('cell5', cellContent);
// cellContentSelector = cellSelectorPre + 6;
// buildTable('cell6', cellContent);
// cellContentSelector = cellSelectorPre + 7;
// buildTable('cell7', cellContent);
// cellContentSelector = cellSelectorPre + 8;
// buildTable('cell8', cellContent);

});

function renderTries(cellId, cellContent) {    
    let myDiv1 = document.createElement('div');
    let cellClass = 'element';
    myDiv1.setAttribute('class', 'multiDigit');
    myDiv1.innerHTML = `        
        <div class=${cellClass}>${cellContent[0]}</div>
        <div class=${cellClass}>${cellContent[1]}</div>
        <div class=${cellClass}>${cellContent[2]}</div>
        <div class=${cellClass}>${cellContent[3]}</div>
        <div class=${cellClass}>${cellContent[4]}</div>
        <div class=${cellClass}>${cellContent[5]}</div>
        <div class=${cellClass}>${cellContent[6]}</div>
        <div class=${cellClass}>${cellContent[7]}</div>
        <div class=${cellClass}>${cellContent[8]}</div>
        `;
    $(cellId).append(myDiv1);
}

function renderRegularCell(cellId, cellContent) {    
    let myDiv2 = document.createElement('div');
    let cellClass = 'cellDigit';
    myDiv2.setAttribute('class', 'singleDigit');
    myDiv2.innerHTML = `        
        <div class=${cellClass}>${cellContent}</div>
        `;
    $(cellId).append(myDiv2);
}
